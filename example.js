/**
 * Riddle solver
 * @author: Krzysztof Piskorz
 * @param { array } board - Macierz będzie użyta do przeliczania danych
 * @returns { array } - Zwraca macierz wynikową
 */
function riddleSolver(board) {
    //Sprawdzanie typu wejścia - jeśli nie tablicowa to zakończ:
    if(!(Array.isArray(board))){
        return "Niewłaściwy typ wejścia."
    }
    /**
     * Utworzenie stałych w celu przyspieszenia pracy programu (długości są liczone tylko raz)
     * @const {number}
     */
    const width = board[0].length;
    const height = board.length;
    //sprawdzanie długości wymiarów tablicy (wysokość, szerokość):
    if(height < 3 || width < 3){
        return "Tablica jest za krótka.";
    }
    else if(height > 100 || width > 100){
        return "Tablica jest za długa.";
    }
    //sprawdzanie stałości wymiarów tablicy (czy wszystkie podtablice mają tę samą długość)
    /**
     * Wartość służąca do iteracji po pierwszej warstwie pętli
     * @type {number}
     */
    let i;
    /**
     * @type {boolean}
     */
    let validBoard = true;
    for(i=1; i<height; i++){
        if(board[i].length != width){
            validBoard = false;
        }
    }
    if(!validBoard){
        return "Tablica o nierównych krawędziach.";
    }
    //
    /**
     * Tworzenie głębokiej kopii pierwotnej tablicy
     * @type {array}
     */
    let clone = JSON.parse(JSON.stringify(board));
    /**
     * Wartość służąca do iteracji po drugiej warstwie pętli
     * @type {number}
     */
    let j;
    /**
     * Flaga określająca status wykonania obliczeń.
     * @type {boolean}
     */
    let done = false;
    //Pętla działająca dopóki kopia i tablica pierwotna nie są takie same
    while(!done){
        /**
        * Klonowanie obecnej kopii do tabeli głównej
        * @type {array}
        */
        board = JSON.parse(JSON.stringify(clone));
        //Sprawdzanie długości łańcuchów znaków w rzędach:
        for (i=0; i<height; i++){
            /**
            * Określanie obecnej długości łańcucha
            * @type {number}
            */
            var chainlength = 1;
            /**
            * Konwersja obecnego rzędu do stanu:
            * lengths - długość ciągu
            * positions - pozycja początkowa ciągu
            * @type {array}
            */
            var lengths = [];
            var positions = [];
            positions.push(0);
            /**
             * Obliczanie pozycji i długości ciągów.
             */
            for(j=1; j<width; j++){
                if (board[i][j-1] == board[i][j]){
                    chainlength++;
                    if(j == width-1){
                        lengths.push(chainlength);
                    }
                }
                else{
                    lengths.push(chainlength);
                    chainlength = 1;
                    positions.push(j);
                }

            }
            //Usuwanie "trójek" lub więcej z rzędów sklonowanej tablicy
            /**
             * Iterator tablicy długości ciągów
             * @type {number}
             */
            var k;
            for(k=0; k<lengths.length; k++){
                if(lengths[k]>2){
                    var l;
                    var pos = positions[k];
                    for(l=0; l<lengths[k]; l++){
                        clone[i][pos+l] = 0;
                    }
                }
            }
        }
        //Sprawdzanie długości łańcuchów znaków w kolumnach:
        for (i=0; i<width; i++){
            /**
            * Określanie obecnej długości łańcucha
            * @type {number}
            */
           var chainlength = 1;
           /**
           * Konwersja obecnej kolumny do stanu:
           * lengths - długość ciągu
           * positions - pozycja początkowa ciągu
           * @type {array}
           */
            var lengths = [];
            var positions = [];
            positions.push(0);
            /**
             * Obliczanie pozycji i długości ciągów.
             */
            for(j=1; j<height; j++){
                if (board[j-1][i] == board[j][i]){
                    chainlength++;
                    if(j == width-1){
                        lengths.push(chainlength);
                    }
                }
                else{
                    lengths.push(chainlength);
                    chainlength = 1;
                    positions.push(j);
                }

            }
            //Usuwanie "trójek" lub więcej z kolumn sklonowanej tablicy
            /**
             * Iterator tablicy długości ciągów
             * @type {number}
             */
            var k;
            for(k=0; k<lengths.length; k++){
                if(lengths[k]>2){
                    var l;
                    var pos = positions[k];
                    for(l=0; l<lengths[k]; l++){
                        clone[pos+l][i] = 0;
                    }
                }
            }
            // Opadanie bloków z liczbami
            /**
             * Iterator kolumn
             * @type {number}
             */
            var m;
            /**
             * Tablica zawierająca aktualnie dostępne pozycje zer
             * @type {array}
             */
            var zeros=[];
            for(m = height-1; m>=0; m--){
                if(clone[m][i] == 0){
                    zeros.push(m);
                }
                else{
                    if(zeros.length != 0){
                        clone[zeros.shift()][i]=clone[m][i];
                        clone[m][i]=0;
                        zeros.push(m);
                    }
                }
            }
        }
        //Porównywanie tablicy pierwotnej z jej przetworzoną kopią
        if(JSON.stringify(board) == JSON.stringify(clone)){
            done=true;
        }
    }
    return clone;
}

export default riddleSolver;